<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-zengarden?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// Z
	'zengarden_description' => ' ',
	'zengarden_slogan' => 'Un jardin Zen pour les squelettes'
);
